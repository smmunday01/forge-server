﻿$(document).ready(function () {
    prepareAppBucketTree();
    $('#refreshBuckets').click(function () {
        $('#appBuckets').jstree(true).refresh();
    });

    $('#uploadUri').click(function () {
        _uploadUri();
    });

    $('#createNewBucket').click(function () {
        createNewBucket();
    });

    $('#createBucketModal').on('shown.bs.modal', function () {
        $("#newBucketKey").focus();
    });
});

function showException(exception) {
    if (exception.status == 500) {
        $.growl.error({ message: exception.responseJSON.ExceptionMessage });
    } else {
        $.growl.error({ message: "" + exception.status + ": " + exception.statusText });
    }
}

function createNewBucket() {
    var bucketKey = $('#newBucketKey').val();
    var policyKey = $('#newBucketPolicyKey').val();
    jQuery.post({
        url: '/api/forge/oss/buckets',
        contentType: 'application/json',
        data: JSON.stringify({ 'bucketKey': bucketKey, 'policyKey': policyKey }),
        success: function (res) {
            $('#appBuckets').jstree(true).refresh();
            $('#createBucketModal').modal('toggle');
        },
        error: showException
    });
}

function prepareAppBucketTree() {
    $('#appBuckets').jstree({
        'core': {
            'themes': { "icons": true },
            'data': {
                "url": '/api/forge/oss/buckets',
                "dataType": "json",
                'multiple': false,
                "data": function (node) {
                    return { "id": node.id };
                }
            }
        },
        'types': {
            'default': {
                'icon': 'glyphicon glyphicon-question-sign'
            },
            '#': {
                'icon': 'glyphicon glyphicon-cloud'
            },
            'bucket': {
                'icon': 'glyphicon glyphicon-folder-open'
            },
            'object': {
                'icon': 'glyphicon glyphicon-file'
            }
        },
        "plugins": ["types", "state", "sort", "contextmenu"],
        contextmenu: { items: autodeskCustomMenu }
    }).on('loaded.jstree', function () {
        $('#appBuckets').jstree('open_all');
    }).bind("activate_node.jstree", function (evt, data) {
        if (data != null && data.node != null && data.node.type == 'object') {
            $("#forgeViewer").empty();
            var urn = data.node.id;
            getForgeToken(function (access_token) {
                jQuery.ajax({
                    url: 'https://developer.api.autodesk.com/modelderivative/v2/designdata/' + urn + '/manifest',
                    headers: { 'Authorization': 'Bearer ' + access_token },
                    success: function (res) {
                        if (res.status === 'success') launchViewer(urn);
                        else $("#forgeViewer").html('The translation job still running: ' + res.progress + '. Please try again in a moment.');
                    },
                    error: function (exception) {
                        if (exception.status == 404) {
                            $.growl.warning({ message: 'File is unfinished. Right-click the model and select <strong>Translate</strong> to proceed.' });
                        } else {
                            showException(exception);
                        }
                    }
                });
            })
        }
    });
}

function autodeskCustomMenu(autodeskNode) {
    var items;

    switch (autodeskNode.type) {
        case "bucket":
            items = {
                uploadFile: {
                    label: "Upload file",
                    action: function () {
                        var treeNode = $('#appBuckets').jstree(true).get_selected(true)[0];
                        uploadFile(treeNode);
                    },
                    icon: 'glyphicon glyphicon-cloud-upload'
                },
                uploadUri: {
                    label: "Upload URI",
                    action: function () {
                        var treeNode = $('#appBuckets').jstree(true).get_selected(true)[0];
                        _uploadUri = uploadUri.bind(null, treeNode);
                        $('#uploadUriModal').modal('show');                        
                    },
                    icon: 'glyphicon glyphicon-globe'
                },
                deleteBucket: {
                    label: "Delete",
                    action: function () {
                        var treeNode = $('#appBuckets').jstree(true).get_selected(true)[0];
                        deleteBucket(treeNode);
                    },
                    icon: 'glyphicon glyphicon-remove'
                }
            };
            break;
        case "object":
            items = {
                translateFile: {
                    label: "Translate",
                    action: function () {
                        var treeNode = $('#appBuckets').jstree(true).get_selected(true)[0];
                        translateObject(treeNode);
                    },
                    icon: 'glyphicon glyphicon-eye-open'
                },
                deleteObject: {
                    label: "Delete",
                    action: function () {
                        var treeNode = $('#appBuckets').jstree(true).get_selected(true)[0];
                        deleteObject(treeNode);
                    },
                    icon: 'glyphicon glyphicon-remove'
                }

            };
            break;
    }

    return items;
}

function uploadFile(node) {
    $('#hiddenUploadField').click();
    $('#hiddenUploadField').change(function () {
        if (this.files.length == 0) return;
        var file = this.files[0];
        switch (node.type) {
            case 'bucket':
                var formData = new FormData();
                formData.append('fileToUpload', file);
                formData.append('bucketKey', node.id);

                $.ajax({
                    url: '/api/forge/oss/objects',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        $('#appBuckets').jstree(true).refresh_node(node);                        
                    },
                    error: showException
                })                
                break;
        }
    });
}

function uploadUri(node) {
    // TODO: If I used Angular or React, I would have much better control on these references.
    switch (node.type) {
        case 'bucket':
            var formData = new FormData();
            formData.append('uri', $('#uri').val());
            formData.append('bucketKey', node.id);

            $.ajax({
                url: '/api/forge/oss/objects/uri',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    $('#appBuckets').jstree(true).refresh_node(node);
                },
                error: showException
            });
            break;
    }
}
var _uploadUri;

function deleteBucket(node) {
    $.ajax({
        url: '/api/forge/oss/bucket',
        data: JSON.stringify({ 'bucketKey': node.id }),
        type: 'DELETE',
        contentType: 'application/json',
        success: function (data) {
            $('#appBuckets').jstree('refresh');
        },
        error: showException
    });
}

function translateObject(node) {
    $("#forgeViewer").empty();
    if (node == null) node = $('#appBuckets').jstree(true).get_selected(true)[0];
    var bucketKey = node.parents[0];
    var objectKey = node.id;
    jQuery.post({
        url: '/api/forge/modelderivative/jobs',
        contentType: 'application/json',
        data: JSON.stringify({ 'bucketKey': bucketKey, 'objectName': objectKey }),
        success: function (res) {
            $("#forgeViewer").html('Translation started! Please try again in a moment.');
        },
        error: showException
    });
}

function deleteObject(node) {
    if (node == null) node = $('#appBuckets').jstree(true).get_selected(true)[0];
    var bucketKey = node.parents[0];
    var objectKey = node.id;

    $.ajax({
        url: '/api/forge/oss/object',
        type: 'DELETE',
        contentType: 'application/json',
        data: JSON.stringify({ 'bucketKey': bucketKey, 'objectName': objectKey }),
        success: function (res) {
            $('#appBuckets').jstree('refresh');
        },
        error: showException
    });
}